FROM debian:stretch

LABEL description “Serveur Web Apache2-PHP7”


# Installation des logiciels
RUN apt-get update \
    && apt-get -y --no-install-recommends install apache2 \
    php7.0 \
    php7.0-mysql \
    locales \
    libapache2-mod-security2 \
    modsecurity-crs \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# Configuration de la Timezone Europe/Paris et des locales

ENV LANG=fr_FR.UTF-8  

RUN rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime \
   && dpkg-reconfigure --frontend noninteractive tzdata \
   && sed -ie 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen \
   && echo 'LANG=${LANG}'>/etc/default/locale \
   && dpkg-reconfigure --frontend=noninteractive locales \
   && update-locale LANG=${LANG} 
COPY ./sites-available/ /etc/apache2/sites-available/
RUN a2dissite 000-default
RUN a2enmod proxy
RUN a2enmod proxy_http
RUN a2ensite dvwa.local
RUN mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
RUN sed -i "s|SecRuleEngine DetectionOnly|SecRuleEngine On|g" /etc/modsecurity/modsecurity.conf
EXPOSE 80

CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]

